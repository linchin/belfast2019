/* 
sub queries - must be executed once for every row in outer query
*/
-- uncorrelated (can run on its own)
select name from jobs 
where id not in (select distinct job_id from employees)

select firstname, lastname from employees 
where salary >= ( select avg(salary) from employees )

-- correlated (relates items in sub query to items in outer query)
-- correlated are more expensive in terms of performance
select e.firstname, e.lastname
from employees e
where e.hiredate > (
select p.start_date from projects p
join assignments a on project_id = p.id
join employees e2 on a.employee_id = e.id
where e.id = e2.id
and p.start_date < e.hiredate
)
