/* 
Views - must appear before anything else, and they stay in memory after execution
 */
create view high_earners2 as
select firstname, lastname
from employees
where salary > (select avg(salary) from employees)

-- to use the view run ONLY THIS CODE
select * from high_earners2
select case when buy = 1 then 'buy' else 'sell' end, price from trade